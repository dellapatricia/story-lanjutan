from django.db import models

# Create your models here.
class MyStatus(models.Model):
    name = models.CharField(max_length=100,blank=False)
    status = models.CharField(max_length=3000,blank=False)
    date = models.DateTimeField(auto_now_add=True, null=True)
