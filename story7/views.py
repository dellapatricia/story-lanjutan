from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import MyStatus
from .forms import MyStatusForm

# Create your views here.
def index(request):
    if request.method == "POST":

        form = MyStatusForm(request.POST)

        if form.is_valid():
            name = request.POST['name']
            status = request.POST['status']

            context = {
                'name' : name,
                'status' : status,
            }

        return render(request, "approval.html", context)

    else:
        form = MyStatusForm()
        status = MyStatus.objects.order_by('-id')

        context = {
            'status' : status,
            'form' : form
        }
        return render(request, 'index.html', context)

def approval_view(request):
    if request.method == "POST":

        if request.POST['confirm'] == 'yes':
            name = request.POST['name']
            status = request.POST['status']

            MyStatus.objects.create(name=name, status=status)
            return redirect('index7')


    return render(request, "approval.html")
