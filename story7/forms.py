from django import forms

class MyStatusForm(forms.Form):
    name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={
        'class':'form-control input-form',
        'placeholder' : "Your name",
        'required':'True'
    }))

    status = forms.CharField(max_length=1000, widget=forms.TextInput(attrs={
        'class':'form-control input-form',
        'placeholder' : "Your message",
        'required':'True'
    }))