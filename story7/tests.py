from django.test import TestCase, Client, LiveServerTestCase
from django.http import HttpRequest
from django.urls import resolve
from selenium import webdriver
from .models import MyStatus
from .views import index,approval_view
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from faker import Faker

import time
import os

# Create your tests here.
class Story7UnitTest(TestCase):
    def test_does_landingpage_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_creating_an_object(self):
        status = MyStatus.objects.create(name='Della', status='I am sick')
        self.assertEqual(MyStatus.objects.count(), 1)
    
    def test_check_landing_page_have_form(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<form', html_response)

    def test_does_approval_page_exist(self):
        response = Client().get('/approval/')
        self.assertEqual(response.status_code, 200)

    def test_form_contains_data(self):
        faker = Faker()
        name = faker.name()
        status = faker.text()
        response = self.client.post('/', data={
            'name': name,
            'status': status
        })

    def test_landing_page_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_approval_page_template(self):
        response = Client().get('/approval/')
        self.assertTemplateUsed(response, 'approval.html')

class Story7FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome('/Users/dellapatricia/Documents/storyku/storylanjutan/chromedriver')

    def test_story7_functionality(self):
        self.browser.get(self.live_server_url + '/')
        time.sleep(5)
        self.browser.find_element_by_name('name').send_keys('MY NAME')
        self.browser.find_element_by_name('status').send_keys('MY STATUS')
        self.browser.find_element_by_name('post').click()
        time.sleep(5)

        page_source = self.browser.page_source
        self.assertIn('MY NAME', page_source)
        self.assertIn('MY STATUS', page_source)
        time.sleep(5)
        self.browser.find_element_by_name('post').click()
        time.sleep(8)
        self.browser.quit()

    def tearDown(self):
        self.browser.quit()
