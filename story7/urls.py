from django.contrib import admin
from django.urls import path, include
from .views import index, approval_view

urlpatterns = [
    path('',index,name='index7'),
    path('approval/', approval_view, name='approval_view')
]