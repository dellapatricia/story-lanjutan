from django.test import TestCase, Client, LiveServerTestCase
from django.http import HttpRequest
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from faker import Faker

import time
import os

# Create your tests here.

class Story8UnitTest(TestCase):
    def test_does_landing_page_exist(self):
        response = Client().get('/accordion/')
        self.assertEqual(response.status_code, 200)

class Story8FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome('/Users/dellapatricia/Documents/storyku/storylanjutan/chromedriver')

    def tearDown(self):
        self.browser.quit()

    def test_website(self):
        self.browser.get(self.live_server_url + '/accordion/')

        self.assertIn("MY ACTIVITY", self.browser.page_source)
        self.assertIn("MY EXPERIENCE", self.browser.page_source)
        self.assertIn("MY HOBBY", self.browser.page_source)
        self.assertIn("MY CONTACT", self.browser.page_source)
        self.assertIn('<div class="container', self.browser.page_source)
        self.assertIn('<div class="panel-group', self.browser.page_source)
        self.assertIn('<div class="panel', self.browser.page_source)
        self.assertIn('<div class="panel-heading', self.browser.page_source)
        time.sleep(3)

        self.browser.find_element_by_class_name('satu').click()
        self.assertIn("Saya suka makan, tidur, dan bercoding :) (ga lah)", self.browser.page_source)
        time.sleep(3)

        self.browser.find_element_by_class_name('dua').click()
        time.sleep(3)
        self.assertIn("Staff Open House Fasilkom 2019 dan lainnya.", self.browser.page_source)
        time.sleep(3)

        self.browser.find_element_by_class_name('tiga').click()
        time.sleep(3)
        self.assertIn("Sekarang lagi suka menggambar di wacom.", self.browser.page_source)
        time.sleep(3)

        self.browser.find_element_by_class_name('empat').click()
        time.sleep(3)
        self.assertIn("Follow ya instagramku @dellapatricia", self.browser.page_source)
        time.sleep(3)

def test_mode(self):
        self.browser.get(self.live_server_url + '/accordion/')
        body_background = self.find_element_by_id('body').value_of_css_property('background')
        self.assertIn("black.jpg", body_background)
        time.sleep(3)
        self.browser.find_element_by_class_name("switch").click()
        body_background = self.find_element_by_id('body').value_of_css_property('background')
        self.assertIn("light.jpg", body_background)

def test_up_down_button(self):
        self.browser.get(self.live_server_url + '/accordion/')

        upBtn = self.browser.find_element_by_name('button-up')
        downBtn =  self.browser.find_element_by_name('button-down')
        

        page_source = self.browser.page_source
        print("index of activities is: " + str(page_source.find('MY ACTIVITY')))
        print("index of experiences is: " + str(page_source.find('MY EXPERIENCE')))
        self.assertTrue(page_source.find('MY ACTIVITY') < page_source.find('MY EXPERIENCE'))


        downBtn.click()
        page_source = self.browser.page_source
        print("index of activities is: " + str(page_source.find('MY ACTIVITY')))
        print("index of experiences is: " + str(page_source.find('MY EXPERIENCE')))
        self.assertTrue(page_source.find('MY ACTIVITY') > page_source.find('MY EXPERIENCE'))

        upBtn.click()
        page_source = self.browser.page_source
        print("index of activities is: " + str(page_source.find('MY ACTIVITY')))
        print("index of experiences is: " + str(page_source.find('MY EXPERIENCE')))
        self.assertTrue(page_source.find('MY ACTIVITY') < page_source.find('MY EXPERIENCE'))

