# Repository Story 7 - 10 | PPW

```bash
Name        : Della Patricia
NPM         : 1906399436
Class       : PPW-J
```

## Link Website Herokuapp

Story 7
[https://storylanjutan.herokuapp.com/](https://storylanjutan.herokuapp.com/)

Story 8
[https://storylanjutan.herokuapp.com/accordion/](https://storylanjutan.herokuapp.com/accordion/)

Story 9
[https://storylanjutan.herokuapp.com/books/](https://storylanjutan.herokuapp.com/books/)

Story 10
[https://storylanjutan.herokuapp.com/accounts/login/](https://storylanjutan.herokuapp.com/accounts/login/)

## Status Website

[![pipeline status](https://gitlab.com/dellapatricia/story-lanjutan/badges/master/pipeline.svg)](https://gitlab.com/dellapatricia/story-lanjutan/-/commits/master)
[![coverage report](https://gitlab.com/dellapatricia/story-lanjutan/badges/master/coverage.svg)](https://gitlab.com/dellapatricia/story-lanjutan/-/commits/master)
