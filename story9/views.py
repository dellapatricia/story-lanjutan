from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import JsonResponse
from .models import LikedBook
from django.views.decorators.csrf import csrf_exempt
import json
import requests


def index(request):
    return render(request,'story9/index.html')

@csrf_exempt
def add_like(request):
    data = json.loads(request.body)

    try:
        likedBook = LikedBook.objects.get(bookID= data['id'])
        likedBook.likes +=1
        likedBook.save()

    except LikedBook.DoesNotExist:
        item = data['volumeInfo']
        image = item['imageLinks']

        likedBook = LikedBook.objects.create(
            bookID = data['id'],
            cover = image.get('smallThumnail', '-'),
            title = item['title'],
            author = item.get('authors', '-'),
            likes = 1
        )
    return JsonResponse(likedBook.likes, safe=False) 

