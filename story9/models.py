from django.db import models

# Create your models here.

class LikedBook(models.Model) :
    bookID = models.CharField(max_length=10, primary_key=True)
    cover = models.TextField(blank=True)
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=255, blank=True)
    likes = models.IntegerField(default=0)
