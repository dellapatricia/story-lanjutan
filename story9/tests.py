from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

from faker import Faker

import time
import os
from .views import *

class Story9UnitTest(TestCase):
    def test_books_url_exist(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)
    
    def test_books_page_use_login_function(self):
        function = resolve('/books/')
        self.assertEqual(function.func, index)

    def test_books_page_use_login_template(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'story9/index.html')

class Story9FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome('/Users/dellapatricia/Documents/storyku/storylanjutan/chromedriver')

    def tearDown(self):
        self.browser.quit()

    def test_search_box_show_books_with_name(self):
        self.browser.get(self.live_server_url + '/books/')
        time.sleep(5)
        
        query_input = self.browser.find_element_by_id("books")
        query_input.send_keys("Okay")
        time.sleep(15) # Wait web page to process and showing result


    
    