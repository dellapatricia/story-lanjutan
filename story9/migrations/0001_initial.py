# Generated by Django 3.0.3 on 2020-05-01 11:24

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='LikedBook',
            fields=[
                ('bookID', models.CharField(max_length=10, primary_key=True, serialize=False)),
                ('cover', models.TextField(blank=True)),
                ('title', models.CharField(max_length=100)),
                ('author', models.CharField(blank=True, max_length=255)),
                ('likes', models.IntegerField(default=0)),
            ],
        ),
    ]
