from django.contrib import admin
from django.urls import path, include
from .views import index,add_like

urlpatterns = [
    path('',index,name='index'),
    path('addlike/', add_like, name='add_like')
]