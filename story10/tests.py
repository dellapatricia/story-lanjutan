from django.test import TestCase, Client, LiveServerTestCase
from django.http import HttpRequest
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from django.contrib.auth.models import User
from selenium.webdriver.chrome.options import Options

from faker import Faker

import os
import time

# Create your tests here.
class Story10UnitTest(TestCase):
    def test_does_landingpage_exist(self):
        response = Client().get('/user/')
        self.assertEqual(response.status_code, 200)

    def test_does_loginpage_exist(self):
        response = Client().get('/accounts/login/')
        self.assertEqual(response.status_code,200)

class Story10FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome('/Users/dellapatricia/Documents/storyku/storylanjutan/chromedriver')

    def tearDown(self):
        self.browser.quit()

    def test_login_page_login(self):
        # Create User
        User.objects.create_user('semangka', 'semangkakeren@cihuy.com', 'merahmanis15')

        self.browser.get(self.live_server_url + '/accounts/login/')

        # Wait until page open 
        time.sleep(5)

        username = self.browser.find_element_by_id('id_username')
        username.send_keys("semangka")

        password = self.browser.find_element_by_id('id_password')
        password.send_keys("merahmanis15")

        login_button = self.browser.find_element_by_class_name("login")
        login_button.click()

        # Wait Authentication process and redirect to protected homepage
        time.sleep(5)
        self.assertIn('Hey there, semangka', self.browser.page_source) # Verify username is showed

    def test_login_page_wrong_password(self):
        # Create User
        User.objects.create_user('mangga', 'cape@cape.com', 'capebanget33')

        self.browser.get(self.live_server_url + '/accounts/login/')

        # Wait until page open 
        time.sleep(5)

        username = self.browser.find_element_by_id('id_username')
        username.send_keys("mangga")

        password = self.browser.find_element_by_id('id_password')
        password.send_keys("CAPEBANGET34")

        login_button = self.browser.find_element_by_class_name("login")
        login_button.click()


        time.sleep(5)

        self.assertIn('Username/password not match!', self.browser.page_source)


