$('.button-up').click(function(){
  var thisAccordion = $(this).parent().parent().parent().parent();
  thisAccordion.insertBefore(thisAccordion.prev());
})

$('.button-down').click(function(){
  var thisAccordion = $(this).parent().parent().parent().parent();
  thisAccordion.insertAfter(thisAccordion.next());
})

$( function(){
  $('#accordion').sortable()
})

$("input").change(function() {
  if (this.checked) {
      $('link[href="/static/story8/accordion.css"]').attr('href', '/static/story8/accordion2.css');
      document.getElementById('light').innerHTML = '<strong>Night Mode?</strong>';
  } else {
      $('link[href="/static/story8/accordion2.css"]').attr('href', '/static/story8/accordion.css');
      document.getElementById('light').innerHTML = '<strong>Light Mode?</strong>';
  }
});

