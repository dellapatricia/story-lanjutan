$(document).ready(function() {

  $("#books").on("keyup", function(e) {
      var q = e.currentTarget.value.toLowerCase()
      $.ajax({
          url: "https://www.googleapis.com/books/v1/volumes?q=" + q,
          success: function(data) {
              $('#result').html('')
              var url = "";
              var img = "";
              var title = "";
              var author = "";
              var like = "";
              var button = "";
              var idBook = [];
              for (var i = 0; i < data.items.length; i++) {
                title=$('<h5 class="text-center mt-5">' + data.items[i].volumeInfo.title + '</h5>');  
                author=$('<h5 class="text-center"> By: ' + data.items[i].volumeInfo.authors + '</h5>');
                img = $('<img class="img-fluid mx-auto"><br><h5 class="text-center"><a href=' + data.items[i].volumeInfo.infoLink + '><button type="button" class="btn btn-primary" style="margin: 0 2% 0 2%">Read More</button></h5>');
                like = $('<h5 class="text-center mt-5"><button type="button" onclick="liked('+i+')"class="btn btn-light" id="like'+ i +'">Like</button></h5>'); 	
                url= data.items[i].volumeInfo.imageLinks.thumbnail;
                img.attr('src', url);
                title.appendTo('#result');
                author.appendTo('#result');
                img.appendTo('#result');
                like.appendTo('#result');

                

                // $('#like').click(function () {
                //     if (button != true){
                //         button = true;
                //         idBook = i;
                //         title=$('<h5 class="text-center mt-5">' + data.items[idBook].volumeInfo.title + '</h5>');  
                //         author=$('<h5 class="text-center"> By: ' + data.items[idBook].volumeInfo.authors + '</h5>');
                //         img = $('<img class="img-fluid mx-auto"><br><h5 class="text-center"><a href=' + data.items[idBook].volumeInfo.infoLink + '><button type="button" class="btn btn-primary" style="margin: 0 2% 0 2%">Read More</button></h5>');
                //         url= data.items[idBook].volumeInfo.imageLinks.thumbnail;
                //         img.attr('src', url);
                //     }
                //     title.appendTo('#mylovedbooks');
                //     author.appendTo('#mylovedbooks');
                //     img.appendTo('#mylovedbooks');
                //     $(this).css("background-color", "salmon"); 
                //  });
              }
              


          }
          ,error: function(error) {
            alert("Books not found");
          }
      })
  });
});

var likedBooks =[];
function liked(i){
    var data = JSON.stringify(likedBooks[i]);
    var xhttpr = new XMLHttpRequest();
    xhttpr.onreadystatechange= function(){
        if(this.readyState==4){
            if (this.status ==200){
                var response = JSON.parse(this.responseText)
                console.log('response', response);
                $("#like"+i).html(response);
                

            } else {
                alert("Cannot like the book rn. Pls try again later.")
            }
        }
    }
    xhttpr.open("POST","/addlike/");
    xhttpr.send(data);
    
}


